## IONIC 4 - User Registration
### Instalação do Back-End da aplicação
#### Clone o back-end em uma pasta separada
* git clone https://clawlima@bitbucket.org/ClawTeamOne/nodejs-user-registration.git
* npm i

### Run Back-End
* npm start

--------------------------------------------
### Instalação do Front-End
#### Abra a pasta da aplicação e execute
* npm i

### Run do Front-End
* ionic serve
* ionic cordova run android --device


### Trello do Projeto
* https://trello.com/b/PnywSotY/ionic-4-user-registration