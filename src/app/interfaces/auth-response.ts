export interface AuthResponse {
    profile: {
        id: number,
        email: string,
        access_token: string,
        expires_in: string
    }
}
