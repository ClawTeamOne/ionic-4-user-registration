export interface Profile {
  id: number;
  email: string;
  password: string;
  firstname: string;
  lastname: string;
  cep: number;
  neighborhood: string;
  city: string;
  address: string;
  numberAddress: number;
  uf: string;
}
