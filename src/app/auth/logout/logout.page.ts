import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(
    public router: Router,
    public menuCtrl: MenuController
  ) { }

  ngOnInit() {
    this.logout();
  }

  async logout() {
    localStorage.clear()
    this.router.navigateByUrl('/login');
  }

  ionViewWillEnter() {
   this.menuCtrl.enable(true);
  }

}
