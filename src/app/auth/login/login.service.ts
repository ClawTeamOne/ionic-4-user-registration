import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { tap } from  'rxjs/operators';
import { Observable, BehaviorSubject, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { Profile } from  '../../interfaces/profile';
import { AuthResponse } from  '../../interfaces/auth-response';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  authSubject  =  new  BehaviorSubject(false);

  constructor(
    private http: HttpClient
  ) { }

  login = (auth: Profile): Observable<AuthResponse> => {

      return this.http.post(`${environment.serverUrl}login`, auth).pipe(
      tap(async (res: AuthResponse) => {

        if (res.profile) {
          localStorage.setItem("PROFILE", JSON.stringify(res.profile))
          localStorage.setItem("ACCESS_TOKEN", res.profile.access_token)
          localStorage.setItem("EXPIRES_IN", res.profile.expires_in)
          this.authSubject.next(true);
        }
      })
    );
  }
}
