import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  constructor(
    public router: Router,
    public formBuilder: FormBuilder,
    private loginService: LoginService,
    public menuCtrl: MenuController
  ) {
    this.loginForm = formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required]
    });
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
    if(localStorage.getItem("ACCESS_TOKEN")){
      this.router.navigateByUrl('/home')
      return;
    }
  }

  login = () => {
    if(!this.loginForm.value.email || !this.loginForm.value.password) return;
      this.loginService.login(this.loginForm.value).subscribe(res => {
        if(res)
          this.router.navigateByUrl('/home');
      }, err => {
        console.log(err);
      });
  }

  register = () => {
    this.router.navigateByUrl('/register');
  }
}
