import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

import { CepService } from '../../services/cep.service';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;

  constructor(
    public router: Router,
    public formBuilder: FormBuilder,
    private cepService: CepService,
    public menuCtrl: MenuController,
    private registerService: RegisterService,
    public toastController: ToastController
  ) {
    this.registerForm = formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        cep: ['', Validators.required],
        neighborhood: ['', Validators.required],
        city: ['', Validators.required],
        address: ['', Validators.required],
        numberAddress: ['', Validators.required],
        uf: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    if(localStorage.getItem("ACCESS_TOKEN")){
      this.router.navigateByUrl('/home')
      return;
    }
  }

  async presentToast(status) {
    const toast = await this.toastController.create({
      message: status === 200?'Ação realizada com sucesso!':'Ação não realizada!',
      duration: 2000
    });
    toast.present();
  }

  getAddress = () => {
    if(!Number.isInteger(this.registerForm.value.cep) || this.registerForm.value.cep.toString().length <= 7) return;
    this.cepService.getAddress(this.registerForm.value.cep).subscribe(res => {
      this.registerForm.patchValue({
        neighborhood: res.bairro,
        city: res.localidade,
        address: res.logradouro,
        uf: res.uf
      })
    }, err => {
      console.log(err);
    });
  }

  register = () => {
    if(
      !this.registerForm.value.firstname ||
      !this.registerForm.value.lastname ||
      !this.registerForm.value.email ||
      !this.registerForm.value.password ||
      !this.registerForm.value.cep ||
      !this.registerForm.value.neighborhood ||
      !this.registerForm.value.city ||
      !this.registerForm.value.address ||
      !this.registerForm.value.numberAddress ||
      !this.registerForm.value.uf
    ) return;

    this.registerService.register(this.registerForm.value).subscribe(res => {
      if(res)
        this.router.navigateByUrl('/home')
    }, err => {
      console.log(err);
    });
  }

  login = () => {
    this.router.navigateByUrl('/login')
  }
}
