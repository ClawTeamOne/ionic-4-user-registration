import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule } from '@ionic/angular';

import { RegisterPage } from './register.page';
import { CepService } from '../../services/cep.service';
import { RegisterService } from './register.service';

const routes: Routes = [ 
  {
    path: '',
    component: RegisterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HttpClientModule
  ],
  declarations: [RegisterPage],
  providers: [CepService, RegisterService],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ]
})
export class RegisterPageModule {}
