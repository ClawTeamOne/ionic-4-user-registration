import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CepService {

  constructor(
    private http: HttpClient
  ) { }

  getAddress = (cep): Observable<any> => {
    return this.http.get(`https://viacep.com.br/ws/${cep}/json/`);
  }



}
