import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { tap } from  'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { forkJoin } from 'rxjs';
import { Profile } from  '../interfaces/profile';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(
    private http: HttpClient
  ) { }

  updateProfile = (auth: Profile, id): Observable<any> => {
      return this.http.put<Profile>(`${environment.serverUrl}profile/${id}`, auth)
  }

  deleteProfile = (id): Observable<any> => {
      return this.http.delete(`${environment.serverUrl}profile/${id}`, {});
  }
}
