import { Component } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

import { CepService } from '../services/cep.service';
import { HomeService } from './home.service';
import { Profile } from  '../interfaces/profile';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  profileForm: FormGroup;
  profile: Profile;

  constructor(
    public router: Router,
    public formBuilder: FormBuilder,
    private cepService: CepService,
    public menuCtrl: MenuController,
    private homeService: HomeService,
    public toastController: ToastController,
    private localNotifications: LocalNotifications
  ) {

    this.profileForm = formBuilder.group({
        email: ['', Validators.required],
        password: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        cep: ['', Validators.required],
        neighborhood: ['', Validators.required],
        city: ['', Validators.required],
        address: ['', Validators.required],
        numberAddress: ['', Validators.required],
        uf: ['', Validators.required]
    });
  }

  ngOnInit() {
    if(!localStorage.getItem("PROFILE"))
      return;
    this.profile = JSON.parse(localStorage.getItem("PROFILE"))

    this.profileForm.patchValue({
      email: this.profile.email,
      firstname: this.profile.firstname,
      lastname: this.profile.lastname,
      cep: this.profile.cep,
      neighborhood: this.profile.neighborhood,
      city: this.profile.city,
      address: this.profile.address,
      numberAddress: this.profile.numberAddress,
      uf:this.profile.uf
    })

    this.localNotificationDelay();
  }

  ionViewWillEnter() {
    if(!localStorage.getItem("ACCESS_TOKEN")){
       this.router.navigateByUrl('/login')
       return;
    }
   this.menuCtrl.enable(true);
  }

  localNotificationDelay(){
    this.localNotifications.schedule({
       text: 'Lembre-se de manter sempre seus dados atualizados!',
       trigger: {at: new Date(new Date().getTime() + 3600)},
       led: 'FF0000',
       sound: null
    });
  }

  async presentToast(status) {
    const toast = await this.toastController.create({
      message: status === 200?'Ação realizada com sucesso!':'Ação não realizada!',
      duration: 2000
    });
    toast.present();
  }

  getAddress = () => {
    if(!Number.isInteger(this.profileForm.value.cep) || this.profileForm.value.cep.toString().length <= 7) return;

    this.cepService.getAddress(this.profileForm.value.cep).subscribe(res => {
      this.profileForm.patchValue({
        neighborhood: res.bairro,
        city: res.localidade,
        address: res.logradouro,
        uf: res.uf
      })
    }, err => {
      console.log(err);
    });
  }

  updateProfile = () => {
    if(
      !this.profileForm.value.firstname ||
      !this.profileForm.value.lastname ||
      !this.profileForm.value.email ||
      !this.profileForm.value.cep ||
      !this.profileForm.value.neighborhood ||
      !this.profileForm.value.city ||
      !this.profileForm.value.address ||
      !this.profileForm.value.numberAddress ||
      !this.profileForm.value.uf
    ) return;

    this.homeService.updateProfile(this.profileForm.value, this.profile.id).subscribe(res => {
      if(res.status === 200){
        this.presentToast(res.status)

        console.log(res)

        localStorage.setItem("PROFILE", JSON.stringify(this.profileForm.value))
      }
    }, err => {
      console.log(err);
    });

  }

  deleteProfile = () => {
    this.homeService.deleteProfile(this.profile.id).subscribe(res => {
      if(res.status === 200){
        this.presentToast(res.status)
        this.router.navigateByUrl('/logout')
      }
    }, err => {
      console.log(err);
    });
  }
}
