import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CepService } from '../services/cep.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { HomePage } from './home.page';
import { HomeService } from './home.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ]),
    HttpClientModule
  ],
  declarations: [HomePage],
  providers: [CepService, HomeService, LocalNotifications],
  exports: [
    FormsModule,
    ReactiveFormsModule
  ]
})
export class HomePageModule {}
